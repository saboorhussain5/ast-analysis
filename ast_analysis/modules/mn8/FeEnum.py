# -*- coding: utf-8 -*-
"""
Created on Mon May  1 19:24:38 2023

@author: SyedHussain
"""

class FeEnum:
    BLINK = 10
    WINK_LEFT = 11
    WINK_RIGHT = 12
    RAISE_BROW = 32 
    FURROW_BROW = 64
    SMILE = 128
    CLENCH = 256
    LAUGH = 512
    SMIRK_LEFT = 1024
    SMIRK_RIGHT = 2048
    
    
    COLUMN_NAMES = {
        'blink': 'FE.BlinkWink',
        'wink_left': 'FE.BlinkWink',
        'wink_right': 'FE.BlinkWink',
        'smile': 'FE.LowerFaceAction',
        'clench': 'FE.LowerFaceAction',
        'laugh': 'FE.LowerFaceAction',
        'smirk_left': 'FE.LowerFaceAction',
        'smirk_right': 'FE.LowerFaceAction',
        'raise_brow': 'FE.UpperFaceAction',
        'furrow_brow': 'FE.UpperFaceAction',
        }
    
    
        
    