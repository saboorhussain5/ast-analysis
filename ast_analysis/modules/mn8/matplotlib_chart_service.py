import matplotlib.pyplot as plt
import io
import base64
import numpy as np

class MatplotlibChartService:
    def __draw_lines(self, axes):
        lines = [8,10,12]
        for x in lines:
            axes.axvline(x=x, ymin=0, ymax=1, linewidth=0.5, color='r')
            
            
    
    def __get_Y_ticks_labels_mins(self, y_axis_length):
        no_of_5m_blocks = int(y_axis_length/75)
        
        
        labels = ["0m"]
        ticks = [0]
        for i in range(no_of_5m_blocks):
            labels.append(str((i+1)*5) + "m")
            ticks.append((i+1)*75)
        return ticks, labels
    
    def get_cospar_charts(self, cospar_real, cospar_imag):
        cospar_fig = plt.figure(4, figsize=(4.0,4.0))  
        cospar_fig.clf()    
    
        af3_af4 = cospar_fig.add_axes([0.1,0.1,0.35,0.8],None)
    
    
        af3_af4.imshow(cospar_real, cmap='viridis',extent=[0,35,len(cospar_real),0],aspect=0.4)
        af3_af4.set_title('T7-T8 (Real)')
        af3_af4.set_xlabel('Frequency')
    
            
        y_ticks, y_labels = self.__get_Y_ticks_labels_mins(len(cospar_real))
        af3_af4.set_yticks(y_ticks)
        af3_af4.set_yticklabels(y_labels)
        af3_af4.set_xticks([0,10,20,30])
        af3_af4.set_xticklabels(['4','10','20','30'], fontsize=8)
        self.__draw_lines(af3_af4)
    
    
        af3_af4_img = cospar_fig.add_axes([0.5,0.1,0.35,0.8],None)
    
    
        af3_af4_img.imshow(cospar_imag, cmap='viridis',extent=[0,35,len(cospar_imag),0],aspect=0.4)
        af3_af4_img.set_title('T7-T8 (IPOC)')
        af3_af4_img.set_xlabel('Frequency')
            
        y_ticks, y_labels = self.__get_Y_ticks_labels_mins(len(cospar_imag))
        af3_af4_img.set_yticks(y_ticks)
        af3_af4_img.set_yticklabels(y_labels)
        af3_af4_img.set_xticks([0,10,20,30])
        af3_af4_img.set_xticklabels(['4','10','20','30'], fontsize=8)
        self.__draw_lines(af3_af4_img)
    
        flike = io.BytesIO()
        cospar_fig.savefig(flike)
        b64_custom = base64.b64encode(flike.getvalue()).decode()
        
        return b64_custom
    
    def get_variance_plot(self, df):
        df = df.T
        variance = df.var()
    
        var_fig = plt.figure(4, figsize=(15.5,5.0))
        var_fig.clf()   
        af3_af4 = var_fig.add_axes([0.05,0.1,0.92,0.85],None)
        x = np.arange(len(variance)) / 512
        af3_af4.plot(x, variance, label='variance', color='blue')
     
        af3_af4.legend()
    
        # var_fig.tight_layout()
        flike = io.BytesIO()
        var_fig.savefig(flike)
        variance_plot = base64.b64encode(flike.getvalue()).decode()
        
        return variance_plot
    
    
    def get_gyro_xy_plot(self, motionCorrected):
        gyro_fig = plt.figure(5, figsize=(15.5,5.0))
        gyro_fig.clf()   
        af3_af4 = gyro_fig.add_axes([0.05,0.1,0.92,0.85],None)
        x = np.arange(len(motionCorrected[:,0])) / 512
        af3_af4.plot(x, motionCorrected[:,0], label='gyro_q0', color='blue')
        af3_af4.plot(x, motionCorrected[:,1], label='gyro_q1', color='orange')
        af3_af4.plot(x, motionCorrected[:,2], label='gyro_q2', color='brown')
        af3_af4.plot(x, motionCorrected[:,3], label='gyro_q3', color='purple')
    
    
        af3_af4.legend()
    
        # gyro_fig.tight_layout()
        flike = io.BytesIO()
        gyro_fig.savefig(flike)

        return base64.b64encode(flike.getvalue()).decode()



    def get_gyro_xy_corrected_plot(self, gyro_xy_corrected):
    
        gyro_corrected_fig = plt.figure(6, figsize=(15.5,4.0))
        gyro_corrected_fig.clf()   
        af3_af4 = gyro_corrected_fig.add_axes([0.05,0.1,0.92,0.85],None)
        x = np.arange(len(gyro_xy_corrected)) / 512
        af3_af4.plot(x,gyro_xy_corrected, label='gyro-ang')
        af3_af4.axhline(y=0.2,linestyle='--', linewidth=4, color='r')
        af3_af4.legend()
        af3_af4.set_ylim(0,1.2)
        # gyro_corrected_fig.tight_layout()
        flike = io.BytesIO()
        gyro_corrected_fig.savefig(flike)
        return base64.b64encode(flike.getvalue()).decode()
        


    def get_cospar_a1_plot(self, crospar_a1):
        crospar_a1 = crospar_a1.T
        fig_cospar_a1 = plt.figure(3, figsize=(7.0,0.2))  
        fig_cospar_a1.clf()    
    
        af3_af4 = fig_cospar_a1.add_axes([0,0,1,1],None)
        
    
        af3_af4.imshow(crospar_a1, cmap='viridis',extent=[0,35,len(crospar_a1),0],aspect=0.2)
        af3_af4.set_ylim(16,20)
        af3_af4.axis('off')
    
        flike = io.BytesIO()
        fig_cospar_a1.savefig(flike)
        return base64.b64encode(flike.getvalue()).decode()