import numpy as np
import neurokit2 as nk
from .matplotlib_chart_service import MatplotlibChartService

class Custom_PP:
    def __init__(self):
        self.chart_service = MatplotlibChartService()
    
    def get_variance_epocs(self, df):
        df = df.T
        variance = df.var()

        threshold = variance.std()
        count = 1
        variance_epocs = []
        for i in range(1,len(variance), 512):
            maximum = np.max(variance[i: i+512])
            if maximum >= threshold:
                variance_epocs.append(1)
            else:
                variance_epocs.append(0) 
            
            count+=1
        return variance_epocs
    
    def get_variance_plot(self, df):
        return self.chart_service.get_variance_plot(df)
    
    def get_blinks_epocs(self, eog_signal):
        srate = 128
        
        # time = np.arange(0, len(df_new)/srate, dt) #time vector
        epoc_len = 4
        
        eog_signal = nk.as_vector(eog_signal)  # Extract the only column as a vector
        
        eog_cleaned = nk.eog_clean(eog_signal, sampling_rate=128, method='neurokit')
        # nk.signal_plot([eog_signal, eog_cleaned], labels=["Raw Signal", "Cleaned Signal"])
        
        blinks = nk.eog_findpeaks(eog_cleaned, sampling_rate=128, method="mne")
        
        epoc_blinks_marked = np.zeros(int(len(eog_cleaned)/(srate*epoc_len)))
        
        for ind in blinks:
                epoc_no = ind / (srate*epoc_len)
                if epoc_no.is_integer():
                    index = int(epoc_no)
                else:
                    index = int(epoc_no)+1
        
        
                if  0 <= index <= len(epoc_blinks_marked) - 1:
                    epoc_blinks_marked[index - 1] = 1
                    
        return epoc_blinks_marked
    
    
    def get_motion_data(self, df):
        motion = df.to_numpy(float)
        motion = motion[~np.isnan(motion).any(axis=1)]
        
        motionCorrected = np.empty([len(motion), 4])
        motionCorrected[:,0] = motion[:,0]-motion[0,0]
        motionCorrected[:,1] = motion[:,1]-motion[0,1]
        motionCorrected[:,2] = motion[:,2]-motion[0,2]
        motionCorrected[:,3] = motion[:,3]-motion[0,3]
        
        motionCorrected = np.diff(motionCorrected, axis=0)
        
        
        gyro_q0_max = []
        gyro_q1_max = []
        gyro_q2_max = []
        gyro_q3_max = []
        
        for i in range(0,len(motionCorrected[:,0]), 512):
            gyro_q0_max.append(np.max(motionCorrected[:,0][i: i+512]))
            gyro_q1_max.append(np.max(motionCorrected[:,1][i: i+512]))
            gyro_q2_max.append(np.max(motionCorrected[:,2][i: i+512]))   
            gyro_q3_max.append(np.max(motionCorrected[:,2][i: i+512]))           
                
                                               
        gyro_xy_corrected = np.sqrt(motionCorrected[:,0]**2+motionCorrected[:,1]**2+motionCorrected[:,2]**2+motionCorrected[:,3]**2)
        
        epochs_gyro_xy_corrected = []
        threshold = 0.2
        for i in range(0,len(gyro_xy_corrected), 256):
            maximum = np.max(gyro_xy_corrected[i: i+256])
            if maximum >= threshold:  
                epochs_gyro_xy_corrected.append(1)
            else:
                epochs_gyro_xy_corrected.append(0)
                
        gyro_q0_max = [np.max(motionCorrected[i:i+512, 0]) for i in range(0, len(motionCorrected[:, 0]), 512)]
        gyro_q1_max = [np.max(motionCorrected[i:i+512, 1]) for i in range(0, len(motionCorrected[:, 1]), 512)]
        gyro_q2_max = [np.max(motionCorrected[i:i+512, 2]) for i in range(0, len(motionCorrected[:, 2]), 512)]
        gyro_q3_max = [np.max(motionCorrected[i:i+512, 3]) for i in range(0, len(motionCorrected[:, 3]), 512)]
        
        naps, longest_nap, time_spent_drowsiness = self.__get_drowsiness_data(gyro_xy_corrected)
                
        return (
            self.__compress_array(epochs_gyro_xy_corrected),
            self.chart_service.get_gyro_xy_plot(motionCorrected),
            self.chart_service.get_gyro_xy_corrected_plot(gyro_xy_corrected),
            gyro_q0_max, gyro_q1_max, gyro_q2_max, gyro_q3_max,
            naps, longest_nap, time_spent_drowsiness
        )
    
    
    def __compress_array(self, arr):
        arr = np.array(arr)  # Convert the input list to a NumPy array
        compressed_arr = np.logical_or(arr[::2], arr[1::2]).astype(int)
        return compressed_arr.tolist()


    def __apply_blinks(self, df):
        pass
    
    def __apply_motion_analysis(self, df):
        pass
    
    def __apply_variance(self, df):
        pass
    
    def apply_custom_pp(self, df):
        df = self.__apply_blinks(df)
        df = self.__apply_motion_analysis(df)
        df = self.__apply_variance(df)
        
        return df

    def get_min_sec(self, seconds):
        min = seconds / 60
        min_sec = math.modf(min)
    
        return str(int(min_sec[1])) + 'm ' + str(round(min_sec[0] * 60)) + 's'
    
    def __get_drowsiness_data(self, gyro_xy_corrected):
        
        srate = 128
        ################lucas Code##############
        nEpochs = round(np.prod(gyro_xy_corrected.shape)/((srate/2)*4))
    
        st = 1
        endd = int((srate/2)*4)
        motionEpoched = []
        for i in range(nEpochs):
            #test = round((max(gyro_xy_corrected[st:endd]))*10000)
            test = round((np.mean(gyro_xy_corrected[st:endd]))*10000)
            motionEpoched.append(test)
    
            st = int(st + (srate/2)*4)
            endd = int(endd + (srate/2)*4)
            
        motionEpoched = (np.array(motionEpoched))/10000
    
        motionEpoched[motionEpoched<0.5] = 0.5
        motionEpoched[-1] = 0.5
    
        unique = list(set(motionEpoched))
        if len(unique) == 1 and unique[0] == 0.5:
            naps = []
            longest_nap = '0m 0s'
            time_spent_drowsiness = '0m 0s'
    
        else:    
            motionEpoched_normalized = (motionEpoched - min(motionEpoched)) / (max(motionEpoched) - min(motionEpoched))
            
            segments = []
            start = 0
            end = 0
            for i in range(len(motionEpoched_normalized)):
                if not start: # start is 0
                    if motionEpoched_normalized[i]:
                        start = i
                else:
                    if not motionEpoched_normalized[i]:
                        end = i - 1 
                        segments.append((start, end))
                        start = 0
    
            naps = [period[1] - period[0] for period in segments]
            longest_nap =     (max(naps))
            time_spent_drowsiness = 0
            for epocs in naps:
                time_spent_drowsiness += epocs*4
            time_spent_drowsiness = self.get_min_sec(time_spent_drowsiness)
            
        return naps, longest_nap, time_spent_drowsiness
        
        
        
        
        
        
    