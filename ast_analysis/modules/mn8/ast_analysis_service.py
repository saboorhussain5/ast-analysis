import numpy as np

SAMPLE_RATE_EEG = 128
EPOC_LENGTH_SEC = 4
EPOC_SIZE = SAMPLE_RATE_EEG * EPOC_LENGTH_SEC

class AstAnalysisService:
    def coherency(self, xt, yt, N, srate):
        # STEP 1 - divide data in N equal length blocks for averaging later on
        L = int(np.floor(len(xt) / N))
        xtR = np.reshape(xt, (-1, L))
        ytR = np.reshape(yt, (-1, L))
    
        # STEP 2 - transform to frequency domain
        xF = np.fft.fft(xtR)
        xF = xF.transpose()
    
        yF = np.fft.fft(ytR)
        yF = yF.transpose()
    
        # STEP 3 - Calculate the frequency of the non-aliased output.
        xF = xF[:(len(xF) // 2) + 1, :2]
        yF = yF[:(len(yF) // 2) + 1, :2]
    
        # Frequency axis
        f = np.multiply(srate, np.divide(list(range(0, int(np.ceil(L / 2)))), L))
    
        # STEP 4 - estimate expectations by taking the average over N blocks
        xy = np.sum(np.multiply(xF, np.conjugate(yF)), axis=1) / N
        xx = np.sum(np.multiply(xF, np.conjugate(xF)), axis=1) / N
        yy = np.sum(np.multiply(yF, np.conjugate(yF)), axis=1) / N
    
        result = np.divide(xy, np.sqrt(np.multiply(xx, yy)))
    
        return f, result
    
    
    def create_crospar(self, df, sensor_1, sensor_2, min_freq, max_freq, part='real', repeats = None ):
 
        crospar_coh_a1_p = []
    
        arr = df.to_numpy()
        
        for i in range(0,len(arr), EPOC_SIZE): 
            epoc_sensor_1 = arr[i:i+EPOC_SIZE,df.columns.get_loc(sensor_1)]
            epoc_sensor_2 = arr[i:i+EPOC_SIZE,df.columns.get_loc(sensor_2)]
            
            
            if len(epoc_sensor_1) == EPOC_SIZE and len(epoc_sensor_2) == EPOC_SIZE:
                
                f, Cxy = self.coherency(epoc_sensor_1,epoc_sensor_2, 2, SAMPLE_RATE_EEG)
        
                crospar_indicies_a1_p = [(f>min_freq) & (f<max_freq)]
                
                Cxy = Cxy[:len(f)]
                
                if part == 'real':
                    crospar_coh_a1_p.append(np.abs((Cxy[tuple(crospar_indicies_a1_p)].real)))
                else:
                    crospar_coh_a1_p.append(np.abs((Cxy[tuple(crospar_indicies_a1_p)].imag)))
    
        
        crospar_coh_a1_p_np = np.array(crospar_coh_a1_p)
        
        if repeats:
            crospar_coh_a1_p_np = np.repeat(crospar_coh_a1_p_np, repeats=2, axis=0)
    
        return f, crospar_coh_a1_p_np


    def merge_cospars(self, cospar_1, cospar_2, threshold):
        merged_cospar = np.concatenate((cospar_1, cospar_2), axis = 1)
        merged_cospar[np.where(merged_cospar<threshold)] = threshold
        return merged_cospar


    def get_am_gt_dt(self, row):
        am_mask = (row < 0.8)
        gt_mask = (0.8 <= row) & (row < 0.9)
        dt_mask = (0.9 <= row) & (row < 1)
        
        am = np.sum(row[am_mask]) if np.all(am_mask) else 0
        gt = np.sum(row[gt_mask]) if np.all(gt_mask) else 0
        dt = np.sum(row[dt_mask]) if np.all(dt_mask) else 0
                
        return am, gt, dt

    def get_time_from_epocs(self, epocs):
        seconds = epocs * EPOC_LENGTH_SEC
        minutes, seconds_remaining = divmod(seconds, 60)
        return f"{minutes}m {seconds_remaining}s"