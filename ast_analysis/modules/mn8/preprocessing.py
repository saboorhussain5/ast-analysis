from scipy import signal
import numpy as np
import pandas as pd

class Preprocessing:
    def __apply_bandpass(self, array):
        sos = signal.butter(N=4, Wn=[3,35],btype='band',fs=128, analog=False,output='sos')
        filtered = signal.sosfilt(sos, array)
        
        return filtered
    
    def get_preprocessed_data(self, df):
        cols_to_process = df.loc[:, ['EEG.T7', 'EEG.T8']].to_numpy()
        processed_cols = np.apply_along_axis(self.__apply_bandpass, 0, cols_to_process)
        processed_df = pd.DataFrame(processed_cols, columns=['EEG.T7', 'EEG.T8'])
        df.loc[:, ['EEG.T7', 'EEG.T8']] = processed_df
        return df



    
