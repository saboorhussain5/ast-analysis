import pandas as pd
import numpy as np
import time
import io, csv
import math

from .js_service import JsService
from .virya_scanning_service import ViryaScanningService
from .mapper import Mapper
from .ast_analysis_service import AstAnalysisService
from .matplotlib_chart_service import MatplotlibChartService
from .preprocessing import Preprocessing
from .custom_preprocessing import Custom_PP



class Mn8Analysis:

    def get_analysis(self, df_raw, df_fac, session_record):

        # df_raw = pd.read_csv("sample_data/Mn8.csv")
        # df_fac = pd.read_csv("sample_data/parv@qntm.app_Apr 13, 2023_6_42 AM_export_fac.csv")

        js_service = JsService()
        scanning = ViryaScanningService()
        mapper = Mapper()
        ast_service = AstAnalysisService()
        chart_service = MatplotlibChartService()
        preprocessing = Preprocessing()
        custom_preprocessing = Custom_PP()


        df = df_raw
        df = mapper.merge_dfs(df_raw, df_fac)
        df = mapper.map_to_sample_rate(df)

        remainder = len(df) % 512

        # If there are extra rows, remove them
        if remainder != 0:
            df = df.iloc[:-remainder]


        attention = df.groupby(df.index // 512)['PM.Attention.Raw'].mean().tolist()
        cog_stress = df.groupby(df.index // 512)['PM.CognitiveStress.Raw'].mean().tolist()


        variance_epocs = custom_preprocessing.get_variance_epocs(df.loc[:, ['EEG.T7', 'EEG.T8']])
        variance_plot = custom_preprocessing.get_variance_plot(df.loc[:, ['EEG.T7', 'EEG.T8']])
        blinks_epocs = custom_preprocessing.get_blinks_epocs(df['EEG.T7']) 
        motion_epocs, gyro_xy_plot, gyro_xy_corrected_plot, gyro_q0_max, gyro_q1_max, gyro_q2_max, gyro_q3_max, naps, longest_nap, time_spent_drowsiness = custom_preprocessing.get_motion_data(df.loc[:, ['MOT.Q0', 'MOT.Q1', 'MOT.Q2', 'MOT.Q3']])

        blinks_epocs_js, variance_epocs_js, motion_epocs_js = js_service.get_artifacts_epocs_js_data(blinks_epocs, variance_epocs, motion_epocs)


        interpolation_blocks, gap_blocks, gapped_epocs, epoc_v_interpolated_count = scanning.get_interpolation_gap_data(df)

        artifacts_list = scanning.get_artifacts_lists(df)

        df = scanning.apply_interpolation(df, interpolation_blocks, ['EEG.T7', 'EEG.T8'])

        df = preprocessing.get_preprocessed_data(df)

        _, cospar_a1_real = ast_service.create_crospar(df, 'EEG.T7', 'EEG.T8', 7, 11, 'real')

        _, cospar_a1_p_real = ast_service.create_crospar(df, 'EEG.T7', 'EEG.T8', 8, 35, 'real')

        _, cospar_a1_p_imag = ast_service.create_crospar(df, 'EEG.T7', 'EEG.T8', 8, 35, 'imag')

        _, cospar_th_real = ast_service.create_crospar(df, 'EEG.T7', 'EEG.T8', 4, 8, 'real')

        _, cospar_th_imag = ast_service.create_crospar(df, 'EEG.T7', 'EEG.T8', 4, 8, 'imag')

        cospar_real = ast_service.merge_cospars(cospar_th_real, cospar_a1_p_real, 0.8)
        cospar_imag = ast_service.merge_cospars(cospar_th_imag, cospar_a1_p_imag, 0.8)

        cospar_real_imag_charts = chart_service.get_cospar_charts(cospar_real, cospar_imag)


        intensity_over_fz = np.nan_to_num(cospar_real, nan=0).mean(axis=0)
        broadband_coherence = np.nan_to_num(cospar_real, nan=0).sum(axis=1)      

        sum_cospar_a1 = np.nan_to_num(cospar_real, nan=0).sum(axis=1)
        mean_cospar_a1 = np.nan_to_num(cospar_real, nan=0).mean(axis=1)
        med_cospar_a1 = np.nan_to_num( np.nanmedian(cospar_a1_real, axis=1), nan=0)

        am_all, gt_all, dt_all = np.apply_along_axis(ast_service.get_am_gt_dt, axis=1, arr=cospar_a1_real).T

        am_mean = (mean_cospar_a1<0.8).astype(int)
        gt_mean = ((0.8 <= mean_cospar_a1) & (mean_cospar_a1 < 0.9)).astype(int)
        dt_mean = ((0.9 <= mean_cospar_a1) & (mean_cospar_a1 < 1)).astype(int)

        am_med = (med_cospar_a1<0.8).astype(int)
        gt_med = ((0.8 <= med_cospar_a1) & (med_cospar_a1 < 0.9)).astype(int)
        dt_med = ((0.9 <= med_cospar_a1) & (med_cospar_a1 < 1)).astype(int)


        formatted_lists_tooltip_header = js_service.format_lists([
            dt_all,
            dt_mean,
            dt_med,
            sum_cospar_a1,
            broadband_coherence,
            
            artifacts_list["epoc"]["cq"]["cq_t8"],
            artifacts_list["epoc"]["cq"]["cq_overall"],
            artifacts_list["epoc"]["eeg"]["interpolated"],

            artifacts_list["epoc"]["eq"]["eq_t8"],
            artifacts_list["epoc"]["eq"]["eq_overall"],
            
            artifacts_list["second"]["cq"]["cq_t8"],
            artifacts_list["second"]["cq"]["cq_overall"],
            artifacts_list["second"]["eeg"]["interpolated"],

            artifacts_list["second"]["eq"]["eq_t8"],
            artifacts_list["second"]["eq"]["eq_overall"],
            gyro_q0_max, attention, cog_stress

            ])



        (dt_all_js, dt_mean_js, dt_med_js, sum_cospar_a1_js, broadband_coherence_js,
         cq_t8_epoc_js, cq_overall_epoc_js, interpolated_epoc_js, eq_t8_epoc_js,
          eq_overall_epoc_js, cq_t8_second_js, cq_overall_second_js, interpolated_second_js, eq_t8_second_js,
          eq_overall_second_js, gyro_q0_max_js, attention_js, cog_stress_js) = formatted_lists_tooltip_header




        formatted_lists_tooltip_wo_header = js_service.format_lists([am_all, gt_all, am_mean, gt_mean,
                      am_med, gt_med, mean_cospar_a1,
                      med_cospar_a1, artifacts_list["epoc"]["cq"]["cq_t7"],
                      artifacts_list["epoc"]["eq"]["eq_t7"],
                      artifacts_list["second"]["cq"]["cq_t7"],    
                      artifacts_list["second"]["eq"]["eq_t7"], 
                      epoc_v_interpolated_count,
                      gyro_q1_max, gyro_q2_max, gyro_q3_max
                      ], tooltip_header=False)

        (am_all_js, gt_all_js, am_mean_js, gt_mean_js, am_med_js, gt_med_js, mean_cospar_a1_js, med_cospar_a1_js,
         eq_t7_epoc_js, cq_t7_epoc_js,  eq_t7_second_js, cq_t7_second_js, epoc_v_interpolated_count_js,
         gyro_q1_max_js, gyro_q2_max_js, gyro_q3_max_js) = formatted_lists_tooltip_wo_header



        return {
                'status': '1',
                'session_id': session_record.id,
                'custom_fr_chart': cospar_real_imag_charts,
                'am_arr_js': am_all_js,
                'gt_arr_js': gt_all_js,
                'dt_arr_js': dt_all_js,
                'am_arr_mean_js':am_mean_js,
                'gt_arr_mean_js':gt_mean_js,
                'dt_arr_mean_js':dt_mean_js,
                'am_arr_med_js':am_med_js,
                'gt_arr_med_js':gt_med_js,
                'dt_arr_med_js':dt_med_js,
                'bb_2_js':broadband_coherence_js,
                'bb_2_max': max(broadband_coherence, default=1),
                'bb_2_min': min(broadband_coherence, default=0),
                'sum_merged_matrix_af3_af4_ast_js': js_service.format_intensity_over_fz(intensity_over_fz),
                'sum_merged_matrix_af3_af4_ast_max': max(intensity_over_fz),
                'sum_merged_matrix_af3_af4_ast_min': min(intensity_over_fz),
                'sum_merged_matrix_af3_af4_a1_js': js_service.format_intensity_over_fz(intensity_over_fz),
                'sum_merged_matrix_af3_af4_a1_max': max(intensity_over_fz),
                'sum_merged_matrix_af3_af4_a1_min': min(intensity_over_fz),
                'mean_cospar_a1_js':mean_cospar_a1_js,
                'med_cospar_a1_js':med_cospar_a1_js,
                'sum_cospar_a1_js': sum_cospar_a1_js,
                'cq_drops': scanning.get_artifacts_drop_time(df, 'CQ.Overall', "<", 100),
                'total_time': scanning.get_time_from_seconds(len(df) / 128),
                'am_time': ast_service.get_time_from_epocs(np.count_nonzero(am_all)),
                'gt_time': ast_service.get_time_from_epocs(np.count_nonzero(gt_all)),
                'dt_time': ast_service.get_time_from_epocs(np.count_nonzero(dt_all)),
                'me_time': ast_service.get_time_from_epocs(np.count_nonzero(dt_all)),
                'am_time_mean': ast_service.get_time_from_epocs(np.count_nonzero(am_mean)),
                'gt_time_mean': ast_service.get_time_from_epocs(np.count_nonzero(gt_mean)),
                'dt_time_mean': ast_service.get_time_from_epocs(np.count_nonzero(dt_mean)),
                'am_time_med': ast_service.get_time_from_epocs(np.count_nonzero(am_med)),
                'gt_time_med': ast_service.get_time_from_epocs(np.count_nonzero(gt_med)),
                'dt_time_med': ast_service.get_time_from_epocs(np.count_nonzero(dt_med)),
                'is_force_stop': session_record.is_force_stop,
                'dive_time': str(session_record.session_date) + ' ' + str(session_record.session_time),
                'band_id': session_record.band_id,
                'is_exp': "0",
                'eq_t7_epoc_js':eq_t7_epoc_js,
                'eq_t8_epoc_js':eq_t8_epoc_js,
                'eq_overall_epoc_js':eq_overall_epoc_js,
                'interpolated_epoc_js': interpolated_epoc_js,
                'cq_t7_epoc_js':cq_t7_epoc_js,
                'cq_t8_epoc_js':cq_t8_epoc_js,
                'cq_overall_epoc_js':cq_overall_epoc_js,
                'eq_t7_second_js':eq_t7_second_js,
                'eq_t8_second_js':eq_t8_second_js,
                'eq_overall_second_js':eq_overall_second_js,
                'interpolated_second_js': interpolated_second_js,
                'cq_t7_second_js':cq_t7_second_js,
                'cq_t8_second_js':cq_t8_second_js,
                'cq_overall_second_js':cq_overall_second_js,
                'epoc_v_interpolated_count_js': epoc_v_interpolated_count_js,
                'gyro_xy_plot': gyro_xy_plot,
                'gyro_xy_corrected_plot': gyro_xy_corrected_plot,
                'fig_cospar_a1': chart_service.get_cospar_a1_plot(cospar_real),
                'varResult_plot': variance_plot,
                'gyro_q0_max_js': gyro_q0_max_js, 
                'gyro_q1_max_js': gyro_q1_max_js, 
                'gyro_q2_max_js': gyro_q2_max_js,
                'gyro_q3_max_js': gyro_q3_max_js,
                'blinks_epocs_js': blinks_epocs_js,
                'variance_epocs_js': variance_epocs_js,
                'motion_epocs_js': motion_epocs_js,
                'naps' : len(naps),
                'longest_nap': longest_nap,
                'time_spent_drowsiness': time_spent_drowsiness,
                'attention_js': attention_js,
                'cog_stress_js': cog_stress_js
                }




