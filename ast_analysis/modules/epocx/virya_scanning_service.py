import numpy as np


SAMPLE_RATE_EEG = 128
EPOC_LENGTH_SEC = 4
EPOC_SIZE = SAMPLE_RATE_EEG * EPOC_LENGTH_SEC

class ViryaScanningService:
    def __get_block_epoch_nums(self, df_block, epoch_size=512):
        start_idx, end_idx = df_block
        start_epoch = int(start_idx / epoch_size) + 1   # Starting the epoc no from 1
        end_epoch = int(end_idx / epoch_size) + 1   # Starting the epoc no from 1
        return list(range(start_epoch, end_epoch+1))

    def __get_block_epoch_counts(self, df_block, epoch_size=512):
        start_idx, end_idx = df_block
        start_epoch = int(start_idx / epoch_size) + 1
        end_epoch = int(end_idx / epoch_size) + 1 
        
        epoch_counts = {}
        for epoch in range(start_epoch, end_epoch+1):
            epoch_start = max(start_idx, (epoch-1)*epoch_size)
            epoch_end = min(end_idx, epoch*epoch_size-1)
            epoch_counts[epoch] = epoch_end - epoch_start + 1
        
        return epoch_counts


    def __get_epoc_count_form_data(self, df):
        return int(len(df) / 512)


    def get_interpolation_gap_data(self, df):
        mask = (
              (df['CQ.Overall'] < 100)
        )     
    
        start_indices = mask.index[mask & (~mask.shift().fillna(False))]
    
        # Create a list of distinct blocks that have a length greater than or equal to 128 and no overlap
        gap_blocks = []
        gapped_epocs = []
        interpolation_blocks = []
        epoc_interpolated_count = np.zeros(self.__get_epoc_count_form_data(df))
        for start_idx in start_indices:
            end_idx = start_idx
            while end_idx < len(mask) and mask[end_idx]:
                end_idx += 1
            if end_idx - start_idx >= 128:
                gap_blocks.append((start_idx, end_idx-1))
                gapped_epocs.extend(self.__get_block_epoch_nums((start_idx, end_idx-1)))
            else: 
                interpolation_blocks.append((start_idx, end_idx-1))
                epoch_nums = self.__get_block_epoch_nums((start_idx, end_idx-1))
                if all(0 <= epoch_num < len(epoc_interpolated_count) for epoch_num in epoch_nums):
                    epoc_interpolated_count[epoch_nums] = 1
                
        return interpolation_blocks, gap_blocks, gapped_epocs, list(epoc_interpolated_count)
    
    def apply_interpolation(self, df, interpolation_blocks, cols_to_update):
        for block in interpolation_blocks:
            start_idx, end_idx = block
            block_size = end_idx - start_idx
            
            num_rows_before_avg = min(start_idx, block_size)
            num_rows_after_avg = min(len(df) - end_idx - 1, block_size)
            
            start_avg_idx = start_idx - num_rows_before_avg
            end_avg_idx = end_idx + num_rows_after_avg
            
            col_indices = [df.columns.get_loc(col) for col in cols_to_update]
            
            col_avgs = np.mean(df.iloc[start_avg_idx:end_avg_idx+1, col_indices], axis=0)
                    
            df.iloc[start_idx:end_idx+1, col_indices] = col_avgs.to_numpy()
        
        return df
    
    def get_artifacts_count_per_block(self, df, col, operator, value, block_len):
        counts = []
        if operator == '<':
            counts = [sum(df[col][i:i+SAMPLE_RATE_EEG] < value) for i in range(0, len(df), block_len)]
        elif operator == '>':
            counts = [sum(df[col][i:i+SAMPLE_RATE_EEG] > value) for i in range(0, len(df), block_len)]
        elif operator == '==':
            counts = [sum(df[col][i:i+SAMPLE_RATE_EEG] == value) for i in range(0, len(df), block_len)]
        elif operator == '!=':
            counts = [sum(df[col][i:i+SAMPLE_RATE_EEG] != value) for i in range(0, len(df), block_len)]
        return counts
    
    
    def get_artifacts_count(self, df, col, operator, value):
        if operator == '<':
            count = (df[col] < value).sum()
        elif operator == '>':
            count = (df[col] > value).sum()
        elif operator == '==':
            count = (df[col] == value).sum()
        elif operator == '!=':
            count = (df[col] != value).sum()
        return count
        
    def get_artifacts_drop_time(self, df, col, operator, value):
        count = self.get_artifacts_count(df, col, operator, value)
        sample_rate = 128
        seconds = (count / sample_rate)
        minutes, seconds = divmod(seconds, 60)
        return f"{int(minutes)} m {int(seconds)} s"
    
    
    def get_time_from_seconds(self, seconds):
        minutes, seconds = divmod(seconds, 60)
        return f"{int(minutes)} m {int(seconds)} s"



    def get_artifacts_lists(self, df, block_len=None):
        
        artifacts_count_per_second = {
            'eeg': {
                'interpolated': self.get_artifacts_count_per_block(df, 'EEG.Interpolated', '==', 1, SAMPLE_RATE_EEG)
                },
            'eq': {
                'eq_overall': self.get_artifacts_count_per_block(df, 'EQ.OVERALL', '<', 100, SAMPLE_RATE_EEG),
                'eq_t7': self.get_artifacts_count_per_block(df, 'EQ.T7', '<', 4, SAMPLE_RATE_EEG),
                'eq_t8': self.get_artifacts_count_per_block(df, 'EQ.T8', '<', 4, SAMPLE_RATE_EEG)
                },
            'cq': {
                'cq_overall': self.get_artifacts_count_per_block(df, 'CQ.Overall', '<', 100, SAMPLE_RATE_EEG),
                'cq_t7': self.get_artifacts_count_per_block(df, 'CQ.T7', '<', 4, SAMPLE_RATE_EEG),
                'cq_t8': self.get_artifacts_count_per_block(df, 'CQ.T8', '<', 4, SAMPLE_RATE_EEG)
                },
            }
        
        artifacts_count_in_epoc = {
            'eeg': {
                'interpolated': self.get_artifacts_count_per_block(df, 'EEG.Interpolated', '==', 1, EPOC_SIZE)
                },
            'eq': {
                'eq_overall': self.get_artifacts_count_per_block(df, 'EQ.OVERALL', '<', 100, EPOC_SIZE),
                'eq_t7': self.get_artifacts_count_per_block(df, 'EQ.T7', '<', 4, EPOC_SIZE),
                'eq_t8': self.get_artifacts_count_per_block(df, 'EQ.T8', '<', 4, EPOC_SIZE)
                },
            'cq': {
                'cq_overall': self.get_artifacts_count_per_block(df, 'CQ.Overall', '<', 100, EPOC_SIZE),
                'cq_t7': self.get_artifacts_count_per_block(df, 'CQ.T7', '<', 4, EPOC_SIZE),
                'cq_t8': self.get_artifacts_count_per_block(df, 'CQ.T8', '<', 4, EPOC_SIZE)
                }
            }
        
        artifacts = {
            'second': artifacts_count_per_second, 
            'epoc': artifacts_count_in_epoc
            }
        
        if not block_len:
            return artifacts
        return artifacts[block_len]