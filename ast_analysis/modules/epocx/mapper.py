import pandas as pd
class Mapper:
    def map_to_sample_rate(self, df):
        df = df.apply(pd.to_numeric, errors='coerce')

        for col in df.columns:
            df[col] = df[col].bfill()
            df[col].fillna(method='ffill', inplace=True)
        
        return df
    
    def merge_dfs(self, primary_df, secondary_df):
        for col in secondary_df.columns:
            primary_df[col] = secondary_df[col]
    
        return primary_df
