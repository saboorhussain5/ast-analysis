import math

SAMPLE_RATE_EEG = 128
EPOC_LENGTH_SEC = 4
EPOC_SIZE = SAMPLE_RATE_EEG * EPOC_LENGTH_SEC


class JsService:
    def get_time_from_epocs(self, epocs):
        seconds = epocs * EPOC_LENGTH_SEC
        minutes, seconds_remaining = divmod(seconds, 60)
        return f"{minutes}m {seconds_remaining}s"
    
    def format_lists(self, list_of_lists, tooltip_header=True):
        data_js = []
        if tooltip_header:
            for lst in list_of_lists:
                data_js.append([{'x': (i+1)/15, 'y': value, 
                                 'toolTipContent': """<span style='\"'color: #406A7C;font-weight: bold;'\"'>Epoch: </span>""" + str(i+1) + """<br/><span style='\"'color: #fc03b6;font-weight: bold;'\"'>Time: </span>""" + self.get_time_from_epocs((i+1)*EPOC_LENGTH_SEC) + """<br/><span style='\"'color: {color};'\"'>{name}: </span>{y}"""
                                 } for i, value in enumerate(lst)])
        else:
            for lst in list_of_lists:
                data_js.append([{'x': (i+1)/15, 'y': value, 
                             'toolTipContent': """<span style='\"'color: {color};'\"'>{name}: </span>{y}"""
                             } for i, value in enumerate(lst)])
        
        return data_js

    def format_intensity_over_fz(self, intensity_arr):

        color_mapping = {range(0, 16): 'red', range(16, 21): 'green', range(21, 25): 'yellow',
                         range(25, 41): 'brown', range(41, 61): '#b8a427'}
        
        return  [{'x': (i/2), 'y': intensity_arr[i], 'markerColor': next((color for r, color in color_mapping.items() if i in r), 'orange')}for i in range(len(intensity_arr))]
         

    def get_artifacts_epocs_js_data(self, blinks_epocs, variance_epocs, motion_epocs):
        blinks_epocs_js = []
        variance_epocs_js = []
        motion_epocs_js = []
        
        for i in range(len(blinks_epocs)):
            j = i + 1
            time_in_sec = j * 4
            minutes = math.floor(time_in_sec / 60)
            seconds = time_in_sec - minutes * 60
            min_sec = str(minutes) + ' m ' + str(seconds) + ' s'
    
    
            toolTipContentDynamic1 =  "<span style='\"'color: #406A7C;font-weight: bold;'\"'>Epoch: </span>" + str(j) +"<br/><span style='\"'color: #fc03b6;font-weight: bold;'\"'>Time: </span>" + min_sec + "<br/><span style='\"'color: {color};'\"'>{name}: </span>{y}";
    
            toolTipContentDynamic = "<span style='\"'color: {color};'\"'>{name}: </span>{y}";
    
            variance_epocs_js.append({'x':(i+1)/15,'y': variance_epocs[i], 'toolTipContent':toolTipContentDynamic})
            motion_epocs_js.append({'x':(i+1)/15,'y': motion_epocs[i], 'toolTipContent':toolTipContentDynamic})
            blinks_epocs_js.append({'x':(i+1)/15,'y': blinks_epocs[i], 'toolTipContent':toolTipContentDynamic1})
            
        return blinks_epocs_js, variance_epocs_js, motion_epocs_js
                
            
            
            
            
            