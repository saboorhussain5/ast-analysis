class CustomPP:
    def __apply_blinks(self, df):
        pass
    
    def __apply_motion_analysis(self, df):
        pass
    
    def __apply_variance(self, df):
        pass
    
    def apply_custom_pp(self, df):
        df = self.__apply_blinks(df)
        df = self.__apply_motion_analysis(df)
        df = self.__apply_variance(df)
        
        return df