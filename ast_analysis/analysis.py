# -*- coding: utf-8 -*-
"""
Created on Wed May 17 15:48:22 2023

@author: ASFET - Saboor
"""

# from ast_analysis.modules.epocx.analysis import Analysis
from common.infrastructure.mappers.map_raw_data import MapRawData
import pandas as pd
import numpy as np
import os

class AstAnalysis:
    def __init__(self):
        self.mapper = MapRawData()
        
        
    def get_analysis(self, band_id, dfs):
        
        df = self.mapper.map_data(dfs)
        
        # if "EPOCX" in band_id:
        #     Analysis.get_analysis()
        # pass
    
        return df
    
    
csv_path = os.path.join(os.path.dirname(__file__), '..', 'sample_data', 'epocx', 'data.csv')

raw_data = pd.read_csv(csv_path)

df_list = [raw_data[col] for col in raw_data.columns]

ast_analysis = AstAnalysis()
df = ast_analysis.get_analysis("EPOCX", df_list)