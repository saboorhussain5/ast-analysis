import pandas as pd
from common.domain.entities.emotiv_constants_enum import EmotivConstantsEnum

class MapRawData:
    def __map_to_sample_rate(self, df):
        df = df.apply(pd.to_numeric, errors='coerce')

        for col in df.columns:
            df[col] = df[col].bfill()
            df[col].fillna(method='ffill', inplace=True)
            
        
        return df
    
    def __merge_dfs(self, dfs):
        merged_df = dfs[0]
        
        for df in dfs[1:]:
            merged_df = pd.concat([merged_df, df], axis=1)  # Concatenate the DataFrames column-wise
        
        return merged_df
    
    
    def __remove_extra_rows(self, df):
        remainder = len(df) % EmotivConstantsEnum.EPOC_SIZE
        if remainder != 0:
            df = df.iloc[:-remainder]
            
        return df
    
    
    def map_data(self, dfs):
        df = self.__merge_dfs(dfs)
        df = self.__remove_extra_rows(df)
        df = self.__map_to_sample_rate(df)
        
        return df
        

